function applyRules(row, col) {
    var numNeighbors = countNeighbors(row, col);
    var numRangeNeighbors = countRangeNeighbors(row, col);
    var range_count_us=numRangeNeighbors[0];
    var range_count_enemy=numRangeNeighbors[1];
    var more_range_neighbors=""
    if (range_count_us>range_count_enemy){
        more_range_neighbors="us"
    }else{
        more_range_neighbors="enemy"
    }
    //console.log(numNeighbors[0]+"p"+ numNeighbors[1])
    if (numNeighbors[1] == 0) {
        if (grid[row][col] == 1) {
            if (numNeighbors[0] < 2) {
                nextGrid[row][col] = 0;
                score -= 0.25;
            } else if (numNeighbors[0] == 2 || numNeighbors[0] == 3) {
                nextGrid[row][col] = 1;
                zoneExpandHandler("us", [row, col])

            } else if (numNeighbors[0] > 3) {
                nextGrid[row][col] = 0;
                score -= 0.25;
            }
        } else if (grid[row][col] == 0) {
            if (numNeighbors[0] == 3) {
                nextGrid[row][col] = 1;
                zoneExpandHandler("us", [row, col])
                score += 0.3;
            }
        }
    } else if (numNeighbors[0] == 0) {
        if (grid[row][col] == 2) {
            if (numNeighbors[1] < 2) {
                nextGrid[row][col] = 0;
                score_enemy -= 0.25;
            } else if (numNeighbors[1] == 2 || numNeighbors[1] == 3) {
                nextGrid[row][col] = 2;
                zoneExpandHandler("enemy", [row, col])

            } else if (numNeighbors[1] > 3) {
                nextGrid[row][col] = 0;
                score_enemy -= 0.25;
            }
        } else if (grid[row][col] == 0) {
            if (numNeighbors[1] == 3) {
                nextGrid[row][col] = 2;
                zoneExpandHandler("enemy", [row, col])
                score_enemy += 0.3;
            }
        }
    } else {
        var numRangeNeighbors = countRangeNeighbors(row, col);
        if (grid[row][col] == 1) {
            if (numRangeNeighbors[0] > numRangeNeighbors[1]) {
                nextGrid[row][col] = 1;
            } else {
                nextGrid[row][col] = 0;
            }
        }else if (grid[row][col]==2){
            if (numRangeNeighbors[1] > numRangeNeighbors[0]){
                nextGrid[row][col] = 2;
            }else{
                nextGrid[row][col] = 0;
            }
        }else{
            
        }
    }
    /*} else if (numNeighbors[0] > numNeighbors[1]) {
        if (grid[row][col] == 2) {
            nextGrid[row][col] = 0;
            score += 0.3;
            score_enemy -= 0.3;
        } else {
            nextGrid[row][col] = 1;
        }
    } else if (numNeighbors[0] < numNeighbors[1]) {
        if (grid[row][col] == 1) {
            nextGrid[row][col] = 0;
            score_enemy + 0.3;
            score -= 0.3;
        } else {
            nextGrid[row][col] = 2;
        }
    } else if (numNeighbors[0] == numNeighbors[1]) {
        nextGrid[row][col] = 0;
    } else {
        nextGrid[row][col] = 0;
    }*/
}

//var console_count_y = 0;
var console_count_b = 0;
function player_console(player, color, log_words, words=0) {
    if (player = "yellow") {
        if (log_words == true) {
            console_count_y = 0;
            document.getElementsByClassName(console_element_y).setAttributes("class", "console_words_y1")
        } else if(color=="green"){
            console_count_y = 3;
            document.getElementsByClassName(console_element_y).setAttributes("class", "console_words_y4")
        }
    } else if (player = "blue") {
        console_count_b = 0;

    }
}