var rows = 113;
var cols = 232;

var playing = false;

var grid = new Array(rows);
var nextGrid = new Array(rows);

var timer;
var reproductionTime = 100;

var score = 10000;
var score_enemy = 10000;
//[y,x]
var pattern0 = [[0, 0]]
var pattern1 = [[0, 0], [0, -1], [0, 1]]
var pattern2 = [[1, 0], [1, 1], [0, 1], [0, 0]];
var pattern3 = [[1, -6], [2, -6], [1, -5], [3, -5], [-4, -4], [1, -4], [3, -4], [4, -4], [-5, -3], [-4, -3], [2, -3], [-6, -2], [-3, -2], [-6, -1], [-5, -1], [-4, -1], [4, 1], [5, 1], [6, 1], [3, 2], [6, 2], [-2, 3], [4, 3], [5, 3], [-4, 4], [-3, 4], [-1, 4], [4, 4], [-3, 5], [-1, 5], [-2, 6], [-1, 6]]

var pattern10 = [[0, 0]]
var pattern_cost_list = [0, 50, 50, 20]
var pattern_cost = 20
var set_pattern = pattern0;
var set_pattern_no = 0;
var pattern_cost_enemy = 20
var set_pattern_enemy = pattern0;
var set_pattern_no_enemy = 20;
var cell_class = "cell";
var hovered_cell = [0, 0];
var hovered_cell_enemy = [100, 100];
var lonely=2;
var overcrowd=3;

const pattern_buttons = document.querySelectorAll('.pattern_button')
for (pattern_button of pattern_buttons) {
    pattern_button.addEventListener('click', function () {
        var clicked_pattern = window["pattern" + (this.id).substring(2, 3)];
        if (set_pattern === clicked_pattern) {
            set_pattern = pattern0;
            set_pattern_no = 0;
            pattern_cost = 20;
            document.querySelector("#" + this.id).classList.remove("pattern_clicked")
        } else {
            const clear_select = document.querySelectorAll('.pattern_button')
            for (clear of clear_select) {
                clear.classList.remove("pattern_clicked")
            }
            set_pattern = clicked_pattern;
            set_pattern_no = parseInt((this.id).substring(2, 3))
            pattern_cost = pattern_cost_list[(this.id).substring(2, 3)]
            document.querySelector("#" + this.id).classList.add("pattern_clicked")
        }
    })
}
const pattern_buttons_enemy = document.querySelectorAll('.pattern_button_enemy')
for (pattern_button of pattern_buttons_enemy) {
    pattern_button.addEventListener('click', function () {
        var clicked_pattern = window["pattern" + (this.id).substring(3, 4)];
        if (set_pattern_enemy === clicked_pattern) {
            set_pattern_enemy = pattern0;
            set_pattern_no_enemy = 0;
            pattern_cost_enemy = 20;
            document.querySelector("#" + this.id).classList.remove("pattern_clicked_enemy")
        } else {
            const clear_select = document.querySelectorAll('.pattern_button_enemy')
            for (clear of clear_select) {
                clear.classList.remove("pattern_clicked_enemy")
            }
            set_pattern_enemy = clicked_pattern;
            set_pattern_no_enemy = parseInt((this.id).substring(3, 4))
            pattern_cost_enemy = pattern_cost_list[(this.id).substring(3, 4)]
            document.querySelector("#" + this.id).classList.add("pattern_clicked_enemy")
        }
    })
}


function initializeGrids() {
    for (var i = 0; i < rows; i++) {
        grid[i] = new Array(cols);
        nextGrid[i] = new Array(cols);
    }
}

function resetGrids() {
    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++) {
            grid[i][j] = 0;
            nextGrid[i][j] = 0;
        }
    }
}

function copyAndResetGrid() {
    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++) {
            grid[i][j] = nextGrid[i][j];
            nextGrid[i][j] = 0;
        }
    }
}

// Initialize
function initialize() {
    createTable();
    initializeGrids();
    resetGrids();
    setupControlButtons();
}

document.querySelector("#read-button").addEventListener('click', function () {
    let file = document.querySelector("#file-input").files[0];
    let reader = new FileReader();
    reader.addEventListener('load', function (e) {
        export_pattern_array=[]
        text = e.target.result;
        document.querySelector("#file-contents").textContent = text;
        var pattern_code = document.querySelector("#file-contents").textContent;
        console.log(pattern_code)
        var rows = pattern_code.split("\n")

        for (row of rows) {
            var codes = row.split("")
            var row_array = []
            var col = 0;
            for (code of codes) {

                if (code == ".") {
                    row_array.push(0);
                } else if (code == "*") {
                    row_array.push(1);
                } else {
                    console.log("x")
                }
                col++;
            }
            grid.push(row_array);
        }//[x,y]

        pattern_width = grid[0].length;
        pattern_height = grid.length;
        middle_cell = [Math.floor(pattern_width / 2), Math.floor(pattern_height / 2)];
        var export_pattern_array=[]
        export_pattern_array_string += "["
        for (var i = 0; i < pattern_height; i++) {
            for (var j = 0; j < pattern_width; j++) {
                //console.log(`${i}+${j}`)
                //console.log(grid[i][j])
                if (grid[i][j] == 1) {
                    //console.log(1)
                    offset_y = i - middle_cell[1];
                    offset_x = j - middle_cell[0];
                    export_pattern_array.push([offset_y, offset_x])//[y,x]
                    export_pattern_array_string += `[${offset_y},${offset_x}]`
                }
                export_pattern_array_string += ","
            }
        }
        /*export_pattern_array_string = export_pattern_array.join("],[")
        document.querySelector("#array-export").innerHTML = `[[${export_pattern_array_string}]]`;

        var export_pattern_array_mirror = []
        for (offset of export_pattern_array) {
            export_pattern_array_mirror.push([offset[0], -offset[1]])
        }
        var export_pattern_array_string_mirror = export_pattern_array_mirror.join("],[")
        document.querySelector("#mirror-array-export-x").innerHTML = `[[${export_pattern_array_string_mirror}]]`;

        var export_pattern_array_mirror = []
        for (offset of export_pattern_array) {
            export_pattern_array_mirror.push([-offset[0], -offset[1]])
        }
        var export_pattern_array_string_mirror = export_pattern_array_mirror.join("],[")
        document.querySelector("#mirror-array-export-y").innerHTML = `[[${export_pattern_array_string_mirror}]]`;

        var export_pattern_array_rotate = []
        for (offset of export_pattern_array) {
            export_pattern_array_rotate.push([offset[1], offset[0]])
        }
        var export_pattern_array_string_rotate = export_pattern_array_rotate.join("],[")
        document.querySelector("#rotate-array-export-y").innerHTML = `[[${export_pattern_array_string_rotate}]]`;

        var export_pattern_array_mirror = []
        for (offset of export_pattern_array_rotate) {//mirror of rotate
            export_pattern_array_mirror.push([-offset[0], offset[1]])
        }
        var export_pattern_array_string_mirror = export_pattern_array_mirror.join("],[")
        document.querySelector("#mirror-rotate-array-export-y").innerHTML = `[[${export_pattern_array_string_mirror}]]`;*/

    });
    reader.readAsText(file);
});

document.getElementById("draw").onclick = function () {
    set_pattern = export_pattern_array;
}

document.getElementById("submit").onclick=function(){
    lonely=parseInt(document.getElementById("lonely").value);
    console.log(lonely)
    overcrowd=parseInt(document.getElementById("overcrowd").value);
    console.log(overcrowd)
}
// Lay out the board
function createTable() {
    var gridContainer = document.getElementById('gridContainer');
    if (!gridContainer) {
        // Throw error
        console.error("Problem: No div for the drid table!");
    }
    var table = document.createElement("table");
    table.setAttribute("id", "t")


    for (var i = 0; i < rows; i++) {
        var tr = document.createElement("tr");
        for (var j = 0; j < cols; j++) {//
            var cell = document.createElement("td");
            cell.setAttribute("id", i + "_" + j);
            cell.setAttribute("class", "cell dead");
            cell.onclick = cellClickHandler;
            cell.onmouseover = cellOverHandler;
            tr.appendChild(cell);
        }
        table.appendChild(tr);
    }
    gridContainer.appendChild(table);
}
var pattern = document.getElementById("")

function cellClickHandler() {
    var clicked_cell = this.id.split("_");
    var clicked_row = clicked_cell[0];
    var clicked_col = clicked_cell[1];
    //var classes = this.getAttribute("class");
    console.log(pattern_cost)
    if (score >= pattern_cost) {
        score -= pattern_cost;
        for (let i = 0; i < set_pattern.length; i++) {
            pattern_cell = set_pattern[i];
            var row = parseInt(clicked_row) + parseInt(pattern_cell[0]);
            var col = parseInt(clicked_col) + parseInt(pattern_cell[1]);
            let pattern_cell_id = `${row}_${col}`;
            var classes = document.getElementById(pattern_cell_id).getAttribute("class");
            if (classes.indexOf("live") > -1) { //check if its class contains"live
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} dead`);
                grid[row][col] = 0;
            } else {
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} live`);
                grid[row][col] = 1;
            }
        }
    } else {
        console.log("no$")
    }
}

function cellOverHandler() {
    updateView();//clear all hovered
    hovered_cell = this.id.split("_");
    var hovered_row = hovered_cell[0];
    var hovered_col = hovered_cell[1];
    for (let i = 0; i < set_pattern.length; i++) {
        pattern_cell = set_pattern[i];
        var row = parseInt(hovered_row) + parseInt(pattern_cell[0]);
        var col = parseInt(hovered_col) + parseInt(pattern_cell[1]);
        if ((row >= 0 && col >= 0) && (row < rows && col < cols)) {
            let pattern_cell_id = `${row}_${col}`;
            var classes = document.getElementById(pattern_cell_id).getAttribute("class");
            if (classes.indexOf("live") > -1) { //check if its class contains"live
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} cell_hovered live`);
            } else {
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} cell_hovered dead`);
            }
        }
    }
}

function directionButtonHandler(direction) {
    var pressed_row = parseInt(hovered_cell[0]);
    var pressed_col = parseInt(hovered_cell[1]);
    updateView();
    if (direction == "no") {

    } else if (direction == "down") {
        pressed_row += 2;
        hovered_cell[0] = parseInt(hovered_cell[0]) + 2;
    } else if (direction == "up") {
        pressed_row -= 2;
        hovered_cell[0] = parseInt(hovered_cell[0]) - 2;
    } else if (direction == "right") {
        pressed_col += 2;
        hovered_cell[1] = parseInt(hovered_cell[1]) + 2;
    } else if (direction == "left") {
        pressed_col -= 2;
        hovered_cell[1] = parseInt(hovered_cell[1]) - 2;
    }
    for (let i = 0; i < set_pattern.length; i++) {
        pattern_cell = set_pattern[i];
        var prow = parseInt(pressed_row) + parseInt(pattern_cell[0]);
        var pcol = parseInt(pressed_col) + parseInt(pattern_cell[1]);
        if (prow < rows && pcol < cols) {
            let pattern_cell_id = `${prow}_${pcol}`;
            var classes = document.getElementById(pattern_cell_id).getAttribute("class");
            if (classes.indexOf("live") > -1) { //check if its class contains"live
                document.getElementById((`${prow}_${pcol}`)).setAttribute("class", `${cell_class} cell_hovered live`);
            } else {
                document.getElementById((`${prow}_${pcol}`)).setAttribute("class", `${cell_class} cell_hovered dead`);
            }
        }
    }
}
function directionButtonHandler_enemy(direction) {
    var pressed_row = parseInt(hovered_cell_enemy[0]);
    var pressed_col = parseInt(hovered_cell_enemy[1]);
    var out_of_bound = false;
    updateView();
    if ((pressed_row < 0 && pressed_col < 0) && (pressed_row > rows && pressed_col > cols)) {
    } else if (direction == "down") {
        pressed_row += 2;
        hovered_cell_enemy[0] = parseInt(hovered_cell_enemy[0]) + 2;
    } else if (direction == "up") {
        pressed_row -= 2;
        hovered_cell_enemy[0] = parseInt(hovered_cell_enemy[0]) - 2;
    } else if (direction == "right") {
        pressed_col += 2;
        hovered_cell_enemy[1] = parseInt(hovered_cell_enemy[1]) + 2;
    } else if (direction == "left") {
        pressed_col -= 2;
        hovered_cell_enemy[1] = parseInt(hovered_cell_enemy[1]) - 2;
    }
    for (let i = 0; i < set_pattern_enemy.length; i++) {
        pattern_cell_enemy = set_pattern_enemy[i];
        var prow = parseInt(pressed_row) + parseInt(pattern_cell_enemy[0]);
        var pcol = parseInt(pressed_col) + parseInt(pattern_cell_enemy[1]);
        if (prow < rows && pcol < cols) {
            let pattern_cell_id = `${prow}_${pcol}`;
            var classes = document.getElementById(pattern_cell_id).getAttribute("class");
            if (classes.indexOf("live") > -1) { //check if its class contains"live
                document.getElementById((`${prow}_${pcol}`)).setAttribute("class", `${cell_class} cell_hovered_enemy live`);
            } else {
                document.getElementById((`${prow}_${pcol}`)).setAttribute("class", `${cell_class} cell_hovered_enemy dead`);
            }
        }

    }
}
function placeButtonHandler() {
    var place_row = hovered_cell[0];
    var place_col = hovered_cell[1];
    //var classes = this.getAttribute("class");
    if (score >= pattern_cost) {
        score -= pattern_cost;
        for (let i = 0; i < set_pattern.length; i++) {
            pattern_cell = set_pattern[i];
            var row = parseInt(place_row) + parseInt(pattern_cell[0]);
            var col = parseInt(place_col) + parseInt(pattern_cell[1]);
            let pattern_cell_id = `${row}_${col}`;
            var classes = document.getElementById(pattern_cell_id).getAttribute("class");
            if (classes.indexOf("live") > -1) { //check if its class contains"live
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} dead`);
                grid[row][col] = 0;
            } else {
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} live`);
                grid[row][col] = 1;
            }
        }
    } else {
        console.log("no$")
    }
}
function placeButtonHandler_enemy() {
    var place_row = hovered_cell_enemy[0];
    var place_col = hovered_cell_enemy[1];
    //var classes = this.getAttribute("class");
    if (score_enemy >= pattern_cost_enemy) {
        score_enemy -= pattern_cost_enemy;
        for (let i = 0; i < set_pattern_enemy.length; i++) {
            pattern_cell_enemy = set_pattern_enemy[i];
            var row = parseInt(place_row) + parseInt(pattern_cell_enemy[0]);
            var col = parseInt(place_col) + parseInt(pattern_cell_enemy[1]);
            let pattern_cell_id = `${row}_${col}`;
            var classes = document.getElementById(pattern_cell_id).getAttribute("class");
            if (classes.indexOf("live_enemy") > -1) { //check if its class contains"live
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} dead`);
                grid[row][col] = 0;
            } else {
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} live_enemy`);
                grid[row][col] = 2;
            }
        }
    } else {
        console.log("no$")
    }
}

function patternButtonHandler(direction) {
    if (direction == "up") {
        if (set_pattern != pattern0) {
            set_pattern_no = parseInt(set_pattern_no) - 1;
            set_pattern = window["pattern" + set_pattern_no]
            pattern_cost = pattern_cost_list[set_pattern_no]
            const clear_select = document.querySelectorAll('.pattern_button')
            for (clear of clear_select) {
                clear.classList.remove("pattern_clicked")
            }
            var pattern_clicked_id = `#pt${set_pattern_no}`
            document.querySelector(`${pattern_clicked_id}`).classList.add("pattern_clicked")
        }
    } else if (direction == "down") {
        if (set_pattern != pattern10) {
            set_pattern_no = parseInt(set_pattern_no) + 1;
            set_pattern = window["pattern" + set_pattern_no]
            pattern_cost = pattern_cost_list[set_pattern_no]
            const clear_select = document.querySelectorAll('.pattern_button')
            for (clear of clear_select) {
                clear.classList.remove("pattern_clicked")
            }
            var pattern_clicked_id = `#pt${set_pattern_no}`
            document.querySelector(pattern_clicked_id).classList.add("pattern_clicked")
        }
    }
}
function patternButtonHandler_enemy(direction) {
    if (direction == "up") {
        if (set_pattern_enemy != pattern0) {
            set_pattern_no_enemy = parseInt(set_pattern_no_enemy) - 1;
            set_pattern_enemy = window["pattern" + set_pattern_no_enemy]
            pattern_cost_enemy = pattern_cost_list[set_pattern_no_enemy]
            const clear_select = document.querySelectorAll('.pattern_button_enemy')
            for (clear of clear_select) {
                clear.classList.remove("pattern_clicked_enemy")
            }
            var pattern_clicked_id = `#ept${set_pattern_no_enemy}`
            document.querySelector(`${pattern_clicked_id}`).classList.add("pattern_clicked_enemy")
        }
    } else if (direction == "down") {
        if (set_pattern_no_enemy != pattern10) {
            set_pattern_no_enemy = parseInt(set_pattern_no_enemy) + 1;
            set_pattern_enemy = window["pattern" + set_pattern_no_enemy]
            pattern_cost_enemy = pattern_cost_list[set_pattern_no_enemy]
            const clear_select = document.querySelectorAll('.pattern_button_enemy')
            for (clear of clear_select) {
                clear.classList.remove("pattern_clicked_enemy")
            }
            var pattern_clicked_id = `#ept${set_pattern_no_enemy}`
            document.querySelector(pattern_clicked_id).classList.add("pattern_clicked_enemy")
        }
    }
}

function updateView() {
    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++) {
            var cell = document.getElementById(i + "_" + j);
            if (grid[i][j] == 0) {
                /*f(document.getElementById(`${i}_${j}`).indexOf("hovered")>-1){
                    cell.setAttribute("class", "cell_hovered dead");
                }*/
                cell.setAttribute("class", `${cell_class} dead`);
            } else {
                if (grid[i][j] == 1) {
                    cell.setAttribute("class", `${cell_class} live`);
                } else if (grid[i][j] == 2) {
                    cell.setAttribute("class", `${cell_class} live_enemy`);
                }
            }
        }
    }//draw hovered cell
    var hovered_row = hovered_cell[0];
    var hovered_col = hovered_cell[1];
    for (let i = 0; i < set_pattern.length; i++) {
        pattern_cell = set_pattern[i];
        var row = parseInt(hovered_row) + parseInt(pattern_cell[0]);
        var col = parseInt(hovered_col) + parseInt(pattern_cell[1]);
        let pattern_cell_id = `${row}_${col}`;
        if ((row >= 0 && col >= 0) && (row < rows && col < cols)) {
            var classes = document.getElementById(pattern_cell_id).getAttribute("class");
            if (classes.indexOf("live") > -1) { //check if its class contains"live
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} cell_hovered live`);
            } else {
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} cell_hovered dead`);
            }
        }
    }
    var hovered_row = hovered_cell_enemy[0];
    var hovered_col = hovered_cell_enemy[1];
    for (let i = 0; i < set_pattern_enemy.length; i++) {
        pattern_cell = set_pattern_enemy[i];
        var row = parseInt(hovered_row) + parseInt(pattern_cell[0]);
        var col = parseInt(hovered_col) + parseInt(pattern_cell[1]);
        let pattern_cell_id = `${row}_${col}`;
        if ((row >= 0 && col >= 0) && (row < rows && col < cols)) {
            var classes = document.getElementById(pattern_cell_id).getAttribute("class");
            if (classes.indexOf("live") > -1) { //check if its class contains"live
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} cell_hovered_enemy live`);
            } else {
                document.getElementById((`${row}_${col}`)).setAttribute("class", `${cell_class} cell_hovered_enemy dead`);
            }
        }
    }
}

function setupControlButtons() {
    // button to start
    var startButton = document.getElementById('start');
    startButton.onclick = startButtonHandler;

    // button to clear
    var clearButton = document.getElementById('clear');
    clearButton.onclick = clearButtonHandler;

    // button to set random initial state
    var randomButton = document.getElementById("random");
    randomButton.onclick = randomButtonHandler;
}

function randomButtonHandler() {
    if (playing) return;
    clearButtonHandler();
    score = 100000;
    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++) {
            var isLive = Math.round(Math.random());
            if (isLive == 1) {
                var cell = document.getElementById(i + "_" + j);
                cell.setAttribute("class", `${cell_class} live_enemy`);
                grid[i][j] = 2;
            }
        }
    }
}

// clear the grid
function clearButtonHandler() {
    console.log("Clear the game: stop playing, clear the grid");
    score = 0;
    document.querySelector('.score').innerHTML = score;

    playing = false;
    var startButton = document.getElementById('start');
    startButton.innerHTML = "Start";
    clearTimeout(timer);

    var cellsList = document.getElementsByClassName("live");
    // convert to array first, otherwise, you're working on a live node list
    // and the update doesn't work!
    var cells = [];
    for (var i = 0; i < cellsList.length; i++) {
        cells.push(cellsList[i]);
    }

    for (var i = 0; i < cells.length; i++) {
        cells[i].classList.remove(cell_class)
        cells[i].classList.add(cell_class)
        cells[i].setAttribute("class", `${cell_class} dead`);
    }
    resetGrids();
}

// start/pause/continue the game
function startButtonHandler() {
    if (playing) {
        console.log("Pause the game");
        playing = false;
        this.innerHTML = "Continue";
        clearTimeout(timer);
    } else {
        console.log("Continue the game");
        playing = true;
        this.innerHTML = "Pause";
        play();
    }
}

// run the life game
function play() {
    computeNextGen();
    document.querySelector('.score_y').innerHTML = Math.floor(score);

    if (playing) {
        timer = setTimeout(play, reproductionTime); //makes game continues playing
    }
}

function computeNextGen() {
    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++) {
            applyRules(i, j);
        }
    }

    // copy NextGrid to grid, and reset nextGrid
    copyAndResetGrid();
    // copy all 1 values to "live" in the table
    updateView();
}

// RULES
// Any live cell with fewer than two live neighbours dies, as if caused by under-population.
// Any live cell with two or three live neighbours lives on to the next generation.
// Any live cell with more than three live neighbours dies, as if by overcrowding.
// Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.




function applyRules(row, col) {
    var numNeighbors = countNeighbors(row, col);
    //console.log(numNeighbors[0]+"p"+ numNeighbors[1])
    if (numNeighbors[1] == 0) {
        if (grid[row][col] == 1) {
            if (numNeighbors[0] < lonely) {
                nextGrid[row][col] = 0;
                score -= 0.25;
            } else if (numNeighbors[0] == 2 || numNeighbors[0] == overcrowd) {
                nextGrid[row][col] = 1;

            } else if (numNeighbors[0] > overcrowd) {
                nextGrid[row][col] = 0;
                score -= 0.25;
            }
        } else if (grid[row][col] == 0) {
            if (numNeighbors[0] == overcrowd) {
                nextGrid[row][col] = 1;
                score += 0.3;
            }
        }
    } else if (numNeighbors[0] == 0) {
        if (grid[row][col] == 2) {
            if (numNeighbors[1] < lonely) {
                nextGrid[row][col] = 0;
                score_enemy -= 0.25;
            } else if (numNeighbors[1] == 2 || numNeighbors[1] == overcrowd) {
                nextGrid[row][col] = 2;

            } else if (numNeighbors[1] > overcrowd) {
                nextGrid[row][col] = 0;
                score_enemy -= 0.25;
            }
        } else if (grid[row][col] == 0) {
            if (numNeighbors[1] == overcrowd) {
                nextGrid[row][col] = 2;
                score_enemy += 0.3;
            }
        }
    } else if (numNeighbors[0] > numNeighbors[1]) {
        if (grid[row][col] == 2) {
            nextGrid[row][col] = 0;
            score += 0.3;
            score_enemy -= 0.3;
        } else {
            nextGrid[row][col] = 1;
        }
    } else if (numNeighbors[0] < numNeighbors[1]) {
        if (grid[row][col] == 1) {
            nextGrid[row][col] = 0;
            score_enemy + 0.3;
            score -= 0.3;
        } else {
            nextGrid[row][col] = 2;
        }
    } else if (numNeighbors[0] == numNeighbors[1]) {
        nextGrid[row][col] = 0;
    } else {
        nextGrid[row][col] = 0;
    }
}

function countNeighbors(row, col) {
    var count = 0;
    var count_enemy = 0;

    if (row - 1 >= 0) {
        if (grid[row - 1][col] == 1) count++;
    }
    if (row - 1 >= 0) {
        if (grid[row - 1][col] == 2) count_enemy++;
    }
    if (row - 1 >= 0 && col - 1 >= 0) {
        if (grid[row - 1][col - 1] == 1) count++;
    }
    if (row - 1 >= 0 && col - 1 >= 0) {
        if (grid[row - 1][col - 1] == 2) count_enemy++;
    }
    if (row - 1 >= 0 && col + 1 < cols) {
        if (grid[row - 1][col + 1] == 1) count++;
    }
    if (row - 1 >= 0 && col + 1 < cols) {
        if (grid[row - 1][col + 1] == 2) count_enemy++;
    }
    if (col - 1 >= 0) {
        if (grid[row][col - 1] == 1) count++;
    }
    if (col - 1 >= 0) {
        if (grid[row][col - 1] == 2) count_enemy++;
    }
    if (col + 1 < cols) {
        if (grid[row][col + 1] == 1) count++;
    }
    if (col + 1 < cols) {
        if (grid[row][col + 1] == 2) count_enemy++;
    }
    if (row + 1 < rows) {
        if (grid[row + 1][col] == 1) count++;
    }
    if (row + 1 < rows) {
        if (grid[row + 1][col] == 2) count_enemy++;
    }
    if (row + 1 < rows && col - 1 >= 0) {
        if (grid[row + 1][col - 1] == 1) count++;
    }
    if (row + 1 < rows && col - 1 >= 0) {
        if (grid[row + 1][col - 1] == 2) count_enemy++;
    }
    if (row + 1 < rows && col + 1 < cols) {
        if (grid[row + 1][col + 1] == 1) count++;
    }
    if (row + 1 < rows && col + 1 < cols) {
        if (grid[row + 1][col + 1] == 2) count_enemy++;
    }
    return [count, count_enemy];
}

function grid_size(size) {
    cell_class = `cell${size}`
    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++) {
            var id = (`${i}_${j}`).toString()
            document.getElementById(id).classList.remove(cell_class)
            document.getElementById(id).classList.add(cell_class)
        }
    }
}

document.getElementById("")


function set_reproduction_time(time) {
    reproductionTime = 1000 - time;
}

document.body.addEventListener('keydown', function (event) {
    if (event.keyCode == 88) {
        placeButtonHandler();
    } else if (event.keyCode == 87) {
        directionButtonHandler("up");
    } else if (event.keyCode == 83) {
        directionButtonHandler("down");
    } else if (event.keyCode == 68) {
        directionButtonHandler("right");
    } else if (event.keyCode == 67) {
        patternButtonHandler("up")
    } else if (event.keyCode == 86) {
        patternButtonHandler("down");
    } else if (event.keyCode == 65) {
        directionButtonHandler("left");
    } else if (event.keyCode == 38) {
        event.preventDefault();
        directionButtonHandler_enemy("up");
    } else if (event.keyCode == 40) {
        event.preventDefault();
        directionButtonHandler_enemy("down");
    } else if (event.keyCode == 37) {
        event.preventDefault();
        directionButtonHandler_enemy("left");
    } else if (event.keyCode == 39) {
        event.preventDefault();
        directionButtonHandler_enemy("right");
    } else if (event.keyCode == 191) {
        placeButtonHandler_enemy();
    } else if (event.keyCode == 188) {
        patternButtonHandler_enemy("up");
    } else if (event.keyCode == 190) {
        patternButtonHandler_enemy("down")
    }
})

// Start everything
window.onload = initialize;

